﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Resource_Management : MonoBehaviour {
    //static reference to this script
    public static Resource_Management thisResource_Management;
    //Resource spawn locations
    public GameObject[] resourceGameObjects;
    //List of spawned items
    public List<GameObject> spawnedObjects;
    [Header("Resource GameObject Settings")]
    //Prefab to spawn
    public GameObject resourceObject;
    //random rnage which resource object to spawn in
    int randomrangeSphere = 0;
    // Use this for initialization
    void Start () {
        resourceGameObjects = GameObject.FindGameObjectsWithTag("Resources");
	}
    void Awake()
    {
        thisResource_Management = this;
    }
    public void SpawnResources()//Loop through and force-brute spawn waves
    {
        for (int i = 0; i < 10; i++) // try 50 times. Brute force approach, randomly try to spawn and make sure its in the Spawnable zone. 
            {
                if (WaveController.thisWaveController.spawnedResources < WaveController.thisWaveController.resourcesToSpawn)
                {
                    randomrangeSphere = Random.Range(0, resourceGameObjects.Length);
                    Vector3 position = resourceGameObjects[randomrangeSphere].transform.position + Random.insideUnitSphere * resourceGameObjects[randomrangeSphere].GetComponent<SphereCollider>().radius;
                    position.y = resourceGameObjects[Random.Range(0, resourceGameObjects.Length)].transform.position.y;
                    RaycastHit hitInfo;
                    if (Physics.Raycast(position + new Vector3(0, 1, 0), Vector3.down, out hitInfo, 10) && hitInfo.collider.tag == "Spawnable")//If we hti a spawnable object spawan the object at a raycasted point location
                    {
                        Debug.DrawRay(position + new Vector3(0, 1, 0), Vector3.down);
                        spawnedObjects.Add((GameObject)Instantiate(resourceObject, hitInfo.point, Quaternion.identity));
                        WaveController.thisWaveController.spawnedResources++;
                        break;
                    }
                }

            }
    }

}
