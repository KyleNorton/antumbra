﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NavMeshResourceManagement : MonoBehaviour {
    //For use if navmesh resource system wants to be implemented
    #region NavmeshResource System
    //Commented out as this can be used if resources want to be spawned on a navmesh
    /*
    public static NavMeshResourceManagement thisNavMesh;
    public GameObject objectToSpawn;
    public List <GameObject> objectList;
    public float range = 10.0f;
    bool breakLoop;
    void Awake()
    {
       thisNavMesh = this;
       breakLoop = false;
    }
    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        do
        {
            for (int i = 0; i < 30; i++)
            {
                Vector3 randomPoint = center + Random.insideUnitSphere * range;
                NavMeshHit hit;
                if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
                {
                    result = hit.position;
                    breakLoop = true;
                    return true;
                }
            }
        } while (!breakLoop);

        result = Vector3.zero;
        return false;
    }
    public void SpawnResources()
    {
        Vector3 point;
        if (RandomPoint(transform.position, range, out point))
        {
            Debug.DrawRay(point, Vector3.up, Color.blue, 1.0f);           
            objectList.Add((GameObject)Instantiate(objectToSpawn, point, Quaternion.identity));
            breakLoop = false;
        }
    }
    void Update()
    {

    }
    */
    #endregion
}
