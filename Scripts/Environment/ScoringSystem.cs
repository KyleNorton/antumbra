﻿using UnityEngine;
using System.Collections;

public class ScoringSystem : MonoBehaviour {
    //static reference to this script
    public static ScoringSystem thisScoringSystem;
    //Current player score
    public float currentScore;
	// Use this for initialization
    void Awake()
    {
        thisScoringSystem = this;
    }
	// Update is called once per frame
	void Update () {
        GameController.thisGameController.scoringBoard.GetComponent<UnityEngine.UI.Text>().text = "Current Score: " + currentScore;//update the score text
    }
    
}
