﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemCollider : MonoBehaviour {

    public bool hitCollider;//Have hit collier
    public float turnOffPowerupObjectTime = 2.0f;//after pickup how long till pickup object turns off
    private float thisDeltaTime = 0.0f;//Used as a timer
    public int maxResourceLimit = 5;//max resource limit
    void Start ()
    {
        hitCollider = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Health.thisHealth.playerHealth > 0)//If player is not dead
        {
            if (thisDeltaTime > 0.0f)
            {
                thisDeltaTime += Time.deltaTime;
                if (thisDeltaTime >= turnOffPowerupObjectTime)//If pickuptime has been met turn off object
                {
                    GameController.thisGameController.resourceLimit.gameObject.SetActive(false);
                    thisDeltaTime = 0.0f;
                }
            }
            if (hitCollider)//if pickup collider has been hit
            {
                if (GameController.thisGameController.currentAmmount < 100)//start filling circle
                {
                    GameController.thisGameController.currentAmmount += GameController.thisGameController.speed * Time.deltaTime;
                    GameController.thisGameController.TextIndicator.GetComponent<Text>().text = ((int)GameController.thisGameController.currentAmmount).ToString() + "%";
                    GameController.thisGameController.TextLoading.gameObject.SetActive(true);
                }
                else
                {
                    if (GameController.thisGameController.resourceCount < maxResourceLimit)//less than max resource limit then pickup object
                    {
                        GameController.thisGameController.TextLoading.gameObject.SetActive(false);
                        GameController.thisGameController.TextIndicator.GetComponent<Text>().text = "Picked Up!";
                        GameController.thisGameController.resourcePickUp = true;
                        GameController.thisGameController.pickupTime += Time.deltaTime;
                        Debug.Log("Picked Up Resource");
                        Destroy(this.gameObject);
                        hitCollider = false;
                        GameController.thisGameController.resourceCount += 1;
                        GameController.thisGameController.resourceText.text = " Resources: " + GameController.thisGameController.resourceCount;
                    }
                    else//Dont pickup if maxresource limit has been met
                    {
                        GameController.thisGameController.resourceLimit.gameObject.SetActive(true);
                        thisDeltaTime += Time.deltaTime;
                    }
                }
                GameController.thisGameController.LoadingBar.GetComponent<Image>().fillAmount = GameController.thisGameController.currentAmmount / 100;
            }
            if (GameController.thisGameController.resourcePickUp)//When resource is picked up
            {
                if (GameController.thisGameController.pickupTime >= turnOffPowerupObjectTime)//turn off object
                {
                    GameController.thisGameController.LoadingCircle.gameObject.SetActive(false);
                    GameController.thisGameController.currentAmmount = 0;
                    hitCollider = false;
                    GameController.thisGameController.resourcePickUp = false;
                    GameController.thisGameController.pickupTime = 0.0f;
                }
                else
                {
                    GameController.thisGameController.pickupTime += Time.deltaTime;
                }
            }
        }
    }
    void OnTriggerEnter(Collider col)//On Collider Entry
    {
        if (Health.thisHealth.playerHealth > 0)
        {
            if (col.gameObject.tag == "Player")//hit collider and start picking up
            {
                GameController.thisGameController.LoadingCircle.gameObject.SetActive(true);
                hitCollider = true;
            }
        }
    }
    void OnTriggerExit(Collider col)//On collider exit
    {
        if (Health.thisHealth.playerHealth > 0)//turn off loading circle
        {
            GameController.thisGameController.LoadingCircle.gameObject.SetActive(false);
            GameController.thisGameController.currentAmmount = 0;
            hitCollider = false;
        }
    }
}
