﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class EnhancedFirstPersonController : MonoBehaviour
{
    //static reference to this player
    public static EnhancedFirstPersonController thisFPC;
    [HideInInspector]
    //the player rig animator
    public Animator playerAnim;
    //walk speed of the player
    public float walkSpeed = 6.0f;
    //run speed of the player
    public float runSpeed = 11.0f;
    // If true, diagonal speed (when strafing + moving forward or back) can't exceed normal move speed; otherwise it's about 1.4 times faster
    public bool limitDiagonalSpeed = true;
    // If checked, the run key toggles between running and walking. Otherwise player runs if the key is held down and walks otherwise
    // There must be a button set up in the Input Manager called "Run"
    public bool toggleRun = false;
    //jump speed of the player
    public float jumpSpeed = 8.0f;
    //player gravity amount
    public float gravity = 20.0f;
    // Units that player can fall before a falling damage function is run. To disable, type "infinity" in the inspector
    public float fallingDamageThreshold = 10.0f;
    // If the player ends up on a slope which is at least the Slope Limit as set on the character controller, then he will slide down
    public bool slideWhenOverSlopeLimit = false;
    // If checked and the player is on an object tagged "Slide", he will slide down it regardless of the slope limit
    public bool slideOnTaggedObjects = false;
    //slide speed of the player
    public float slideSpeed = 12.0f;
    // If checked, then the player can change direction while in the air
    public bool airControl = false;
    // Small amounts of this results in bumping when walking down slopes, but large amounts results in falling too fast
    public float antiBumpFactor = .75f;
    // Player must be grounded for at least this many physics frames before being able to jump again; set to 0 to allow bunny hopping
    public int antiBunnyHopFactor = 1;
    //Where the camera is connected to 
    public GameObject cameraBindComponent;
    //sensitivity of the mouse
    public float mouseSensitivity = 5.0f;
    //how far vertically the cmarea has rotated
    float verticalRotation = 0;
    //up down range of the camera
    public float upDownRange = 5.0f;
    //current speed of the player
    public float currentSpeed;
    //Direction of the player
    private Vector3 moveDirection = Vector3.zero;
    //is player grounded
    private bool grounded = false;
    //this Character Controller
    private CharacterController controller;
    //this Transform
    private Transform myTransform;
    //where to apply player speed to
    public float speed;
    //Used for player sliding
    private RaycastHit hit;
    //The fall start level
    private float fallStartLevel;
    //Is the player falling
    private bool falling;
    //Slide limit of the player
    private float slideLimit;
    //Distance of the ray sued
    private float rayDistance;
    //Contact point for falling
    private Vector3 contactPoint;
    //Can the player control
    private bool playerControl = false;
    //Jump timer between space bar press
    private int jumpTimer;
    void Awake()
    {
        thisFPC = this;
    }
    void Start()
    {
        playerAnim = cameraBindComponent.GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        myTransform = transform;
        speed = walkSpeed;
        rayDistance = controller.height * .5f + controller.radius;
        slideLimit = controller.slopeLimit - .1f;
        jumpTimer = antiBunnyHopFactor;
        playerAnim.SetBool("Idle", true);
    }

    void FixedUpdate()
    {
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");
        // If both horizontal and vertical are used simultaneously, limit speed (if allowed), so the total doesn't exceed normal move speed
        float inputModifyFactor = (inputX != 0.0f && inputY != 0.0f && limitDiagonalSpeed) ? .7071f : 1.0f;
        currentSpeed = controller.velocity.magnitude;
        if(Health.thisHealth.playerHealth > 0)
        { 
            if (grounded)
            {
                if (Mathf.RoundToInt(currentSpeed) == 0 && Health.thisHealth.playerHealth > 0)
                {
                    playerAnim.SetBool("Walk", false);
                    playerAnim.SetBool("Run", false);

                    playerAnim.SetBool("Idle", true);
                }
                bool sliding = false;
                // See if surface immediately below should be slid down. We use this normally rather than a ControllerColliderHit point,
                // because that interferes with step climbing amongst other annoyances
                if (Physics.Raycast(myTransform.position, -Vector3.up, out hit, rayDistance))
                {
                    if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
                        sliding = true;
                }
                // However, just raycasting straight down from the center can fail when on steep slopes
                // So if the above raycast didn't catch anything, raycast down from the stored ControllerColliderHit point instead
                else
                {
                    Physics.Raycast(contactPoint + Vector3.up, -Vector3.up, out hit);
                    if (Vector3.Angle(hit.normal, Vector3.up) > slideLimit)
                        sliding = true;
                }

                // If we were falling, and we fell a vertical distance greater than the threshold, run a falling damage routine
                if (falling)
                {
                    falling = false;
                    if (myTransform.position.y < fallStartLevel - fallingDamageThreshold)
                        FallingDamageAlert(fallStartLevel - myTransform.position.y);
                }

                // If running isn't on a toggle, then use the appropriate speed depending on whether the run button is down
                if (!toggleRun)
                {
                    speed = Input.GetButton("Sprint") ? runSpeed : walkSpeed;
                    if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
                    {
                        if (speed == runSpeed)//If were running change the animations and audio
                        {
                            //AudioHandler.thisAudioHandler.PlayRunning();
                            playerAnim.SetBool("Damage", false);
                            playerAnim.SetBool("Idle", false);
                            playerAnim.SetBool("Walk", false);
                            playerAnim.SetBool("Run", true);

                        }
                        else if (speed == walkSpeed && Mathf.RoundToInt(currentSpeed) == walkSpeed)//If were Walking change the animations and audio
                        {
                            playerAnim.SetBool("Damage", false);
                            playerAnim.SetBool("Idle", false);
                            playerAnim.SetBool("Run", false);
                            playerAnim.SetBool("Walk", true);

                        }

                    }
              
                }

                // If sliding (and it's allowed), or if we're on an object tagged "Slide", get a vector pointing down the slope we're on
                if ((sliding && slideWhenOverSlopeLimit) || (slideOnTaggedObjects && hit.collider.tag == "Slide"))
                {
                    Vector3 hitNormal = hit.normal;
                    moveDirection = new Vector3(hitNormal.x, -hitNormal.y, hitNormal.z);
                    Vector3.OrthoNormalize(ref hitNormal, ref moveDirection);
                    moveDirection *= slideSpeed;
                    playerControl = false;
                }
                // Otherwise recalculate moveDirection directly from axes, adding a bit of -y to avoid bumping down inclines
                else
                {
                    moveDirection = new Vector3(inputX * inputModifyFactor, -antiBumpFactor, inputY * inputModifyFactor);
                    moveDirection = myTransform.TransformDirection(moveDirection) * speed;
                    playerControl = true;
                }

                // Jump! But only if the jump button has been released and player has been grounded for a given number of frames
                if (!Input.GetButton("Jump"))
                    jumpTimer++;
                else if (jumpTimer >= antiBunnyHopFactor)
                {
                    moveDirection.y = jumpSpeed;
                    jumpTimer = 0;
                }
        }
        else
        {
            // If we stepped over a cliff or something, set the height at which we started falling
            if (!falling)
            {
                falling = true;
                fallStartLevel = myTransform.position.y;
            }

            // If air control is allowed, check movement but don't touch the y component
            if (airControl && playerControl)
            {
                moveDirection.x = inputX * speed * inputModifyFactor;
                moveDirection.z = inputY * speed * inputModifyFactor;
                moveDirection = myTransform.TransformDirection(moveDirection);
            }
        }

        // Apply gravity
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller, and set grounded true or false depending on whether we're standing on something
        grounded = (controller.Move(moveDirection * Time.deltaTime) & CollisionFlags.Below) != 0;
    }
    }

    void Update()
    {
        // If the run button is set to toggle, then switch between walk/run speed. (We use Update for this...
        // FixedUpdate is a poor place to use GetButtonDown, since it doesn't necessarily run every frame and can miss the event)
        if (toggleRun && grounded && Input.GetButtonDown("Sprint"))
        {
            speed = (speed == walkSpeed ? runSpeed : walkSpeed);
        }

        if (!Health.thisHealth.isDead && Time.timeScale == 1)
        {
            float rotLeftRight = Input.GetAxis("Mouse X") * mouseSensitivity;
            transform.Rotate(0, rotLeftRight, 0);
            verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
            verticalRotation = Mathf.Clamp(verticalRotation, -upDownRange, upDownRange);
            cameraBindComponent.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);
        }

    }

    // Store point that we're in contact with for use in FixedUpdate if needed
    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        contactPoint = hit.point;
    }

    // If falling damage occured, this is the place to do something about it. You can make the player
    // have hitpoints and remove some of them based on the distance fallen, add sound effects, etc.
    void FallingDamageAlert(float fallDistance)
    {
        print("Ouch! Fell " + fallDistance + " units!");
    }
}