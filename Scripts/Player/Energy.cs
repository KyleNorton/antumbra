﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Energy : MonoBehaviour {
    //static refernence to this script
    public static Energy thisEnergy;
     [Header("Energy Bar Settings")]
     //amount of bars on the energy bar
    public int amountOfBars = 9;
    //how much to decrease energy by
    public float decreaseAmount = 0.25f;
     [Header("FlashLight Bar Settings")]
     //amount of bars in the flashlight bar
    public int amountOfBarsFlash = 9;
    //amount to decrease flashlight by
    public float decreaseAmountFlashLight = 1.5f;

    // Use this for initialization
    void Awake()
    {
        thisEnergy = this;
    }

    public void DecreaseEnergy()//Decrease the energy and bars by the predefined amount
    {
        if(amountOfBars >= 0)
        {
            GameUIController.thisGameUIController.EnergyBarObjects[amountOfBars].fillAmount -= decreaseAmount;
            if (GameUIController.thisGameUIController.EnergyBarObjects[amountOfBars].fillAmount <= 0.0f)
            {
                GameUIController.thisGameUIController.EnergyBarObjects[amountOfBars].gameObject.SetActive(false);
                amountOfBars--;
            }
        }


    }
    public void DecreaseFlashLight()//Decrease the flashlight and bars by the predefined amount
    {
        if (amountOfBarsFlash >= 0)
        {
            GameUIController.thisGameUIController.FlashLightBarObjects[amountOfBarsFlash].fillAmount -= decreaseAmountFlashLight * Time.deltaTime;
            if (GameUIController.thisGameUIController.FlashLightBarObjects[amountOfBarsFlash].fillAmount <= 0.0f)
            {
                GameUIController.thisGameUIController.FlashLightBarObjects[amountOfBarsFlash].gameObject.SetActive(false);
                amountOfBarsFlash--;
            }
        }
    }
    public void Reload()//Update energy on reload of gun
    {
        amountOfBars = 9;
        GameUIController.thisGameUIController.UpdateEnergyBars();
        GameUIController.thisGameUIController.UpdateFlashLightBars();
        GameController.thisGameController.flashlightBar.GetComponentInChildren<Text>().text = "FlashLight";
    }

}
