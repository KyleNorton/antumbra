﻿using UnityEngine;
using System.Collections;

public class Player_Bullet : MonoBehaviour {
    //static reference to this script
    public static Player_Bullet thisPlayerBullet;
    //time till next fire
    private float nextFire;
    [Header("Bullet Settings")]
    //foward force of the bullet(speed)
    public float forwardForceBullet = 10.0f;
    //Fire rate of the gun
    public float fireRate;
    #if UNITY_EDITOR
    [ShowOnly]
    #endif
    //bullets shot
    public int bulletCount = 0;
    //bullet prefab ovject
    public GameObject bulletPrefab;
    //where to spawn the bullet
    public Transform shotSpawn;
    //crosshair object
    public Transform crosshair;
    //amount of damage the bullet has
    public float bulletDamage;
    // Use this for initialization
    //rotation to spawn object at
    public Quaternion rotation;
    void Awake()
    {
        thisPlayerBullet = this;
    }
    void ShootBullet(float speedMult)//Shoot the bullet at a passead in speed multiplier
    {
            Quaternion memternion = Quaternion.LookRotation(crosshair.position - shotSpawn.position) * Quaternion.Euler(0, 90.0f, 0);
            EnhancedFirstPersonController.thisFPC.playerAnim.SetBool("Shoot", true);
            //Mae sure we are only playing the shoot anim
            nextFire = Time.time + fireRate;
            Energy.thisEnergy.DecreaseEnergy();
            //Shooting Below
            GameObject temp_bullet;
            temp_bullet = Instantiate(bulletPrefab, shotSpawn.position, memternion) as GameObject;
            Rigidbody temp_rb;
            temp_rb = temp_bullet.GetComponent<Rigidbody>();
            temp_rb.AddForce(Camera.main.transform.forward * forwardForceBullet * speedMult);
            AudioHandler.thisAudioHandler.PlayWeaponClip();
            Destroy(temp_bullet, 3.0f);
            bulletCount++;

    }
	// Update is called once per frame
	void Update ()
    {

        Debug.DrawRay(shotSpawn.position, crosshair.position - shotSpawn.position, Color.green);
        if (Time.timeScale == 1 && !Health.thisHealth.isDead &&  Input.GetButton("Shoot") && Time.time > nextFire && Energy.thisEnergy.amountOfBars >= 0 &&  EnhancedFirstPersonController.thisFPC.speed == EnhancedFirstPersonController.thisFPC.walkSpeed && 
           !EnhancedFirstPersonController.thisFPC.playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Reload")&& !EnhancedFirstPersonController.thisFPC.playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Run"))//If waling and energy > 0 and not dead and press shoot
        {//Check if we are not dead not paused not realoading or running and have ammo
            ShootBullet(1);
        }
        else
        {
            EnhancedFirstPersonController.thisFPC.playerAnim.SetBool("Shoot", false);

        }

    }

}
