﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Health : MonoBehaviour {
    public static Health thisHealth;
    [Header("Health Bar Settings")]
    public int amountOfBars = 9;
    [HideInInspector]
    public bool isDead = false;
    [Header("Health Settings")]
    public float playerHealth = 100;
    // Use this for initialization
    void Awake()
    {
        thisHealth = this;
    }
    void Start () {
        
    }
    IEnumerator DelayDeath()//Wait for death animation to finish playing and then call the death scene
    {
        EnhancedFirstPersonController.thisFPC.playerAnim.SetBool("Death", true);
        yield return new WaitForSeconds(2.50f);
        Time.timeScale = 0;
        GameController.thisGameController.deathScreen.gameObject.SetActive(true);
        GameController.thisGameController.finalScoreBoard.GetComponent<UnityEngine.UI.Text>().text = "Final Score: " + ScoringSystem.thisScoringSystem.currentScore;
        Cursor.lockState = CursorLockMode.None;
        GameController.thisGameController.LoadingCircle.SetActive(false);
        GameController.thisGameController.LoadingBar.gameObject.SetActive(false);
        GameController.thisGameController.TextIndicator.gameObject.SetActive(false);
        GameController.thisGameController.TextLoading.gameObject.SetActive(false);
        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update () {
		if (playerHealth <= 0 && isDead == false)//if player is dead
		{
            isDead = true;
            StartCoroutine(DelayDeath());
        }
    }
	public void UpdateHealth(float damagemultiplier)//Deal damage to player by a passed in amount
    {
        if (damagemultiplier / 10 > 1)
        {
            for (int i = 0; i < (int)damagemultiplier / 10; i++)
            {
                GameUIController.thisGameUIController.healthBarObjects[amountOfBars].fillAmount -= damagemultiplier / 10;
                if (GameUIController.thisGameUIController.healthBarObjects[amountOfBars].fillAmount <= 0.0f)
                {
                    GameUIController.thisGameUIController.healthBarObjects[amountOfBars].gameObject.SetActive(false);
                    amountOfBars--;
                }
                
            }
        }
        else if(amountOfBars >= 0)
        {         
            GameUIController.thisGameUIController.healthBarObjects[amountOfBars].fillAmount -= damagemultiplier / 10;
            if (GameUIController.thisGameUIController.healthBarObjects[amountOfBars].fillAmount <= 0.0f)
            {
                GameUIController.thisGameUIController.healthBarObjects[amountOfBars].gameObject.SetActive(false);
                amountOfBars--;
            }      
        }
        playerHealth -= damagemultiplier;
        EnhancedFirstPersonController.thisFPC.playerAnim.SetBool("Damage", false);
    }
}
