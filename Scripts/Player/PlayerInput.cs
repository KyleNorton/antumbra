﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour {

    public GameObject clipInGun;
    public GameObject clipInHand;

    bool isZoomed = false;
    bool reloading = false;
    bool powerLightActive;
    float reloadTime =  0.0f;
    [Header("Reload Animation Settings")]
    public float clipOut = 0.4f;
    public float clipIn = 2.2f;
    public float reloadAnimDuration = 3.2f;
    [Header("Camera Zoom Settings")]
    public float zoomFOV = 50f;
    public float normalFOV = 60f;
	void Start() {
        clipInHand.SetActive(false);
    }
    // Update is called once per frame
    void Update() {
        if(isZoomed)
        {
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, zoomFOV, Time.deltaTime * 5);
        }
        else
        {
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, normalFOV, Time.deltaTime * 5);

        }
        if (powerLightActive)
        {
            Energy.thisEnergy.DecreaseFlashLight();
            if (Energy.thisEnergy.amountOfBarsFlash <= 0)
            {
                WorldLighting.thisWorldLighting.powerLight.enabled = !WorldLighting.thisWorldLighting.powerLight.enabled;
                powerLightActive = false;
                GameUIController.thisGameUIController.FlashLightBarObjects[0].fillAmount = 0;
                GameUIController.thisGameUIController.FlashLightBarObjects[0].gameObject.SetActive(false);
                GameController.thisGameController.flashlightBar.GetComponentInChildren<UnityEngine.UI.Text>().text = "N/A";
            }

        }
        if(reloading)
        {
            reloadTime += Time.deltaTime;
            if(reloadTime >= clipOut && reloadTime <= clipIn)
            {
                clipInGun.SetActive(false);
                clipInHand.SetActive(true);              
                GameController.thisGameController.flashlightBar.gameObject.SetActive(false);
            }
            if (reloadTime >= clipIn)
            {
                clipInHand.SetActive(false);
                clipInGun.SetActive(true);
            }

            if (reloadTime >= reloadAnimDuration)
            {
                GameController.thisGameController.flashlightBar.gameObject.SetActive(true);
                GameController.thisGameController.resourceCount -= 1;
                GameController.thisGameController.resourceText.text = " Resources: " + GameController.thisGameController.resourceCount;
                Energy.thisEnergy.Reload();
                EnhancedFirstPersonController.thisFPC.playerAnim.SetBool("Reload", false);
                reloading = false;
                reloadTime = 0.0f;
            }

        }
        if (Time.timeScale == 1 && Input.GetButtonDown("PowerLight") && Energy.thisEnergy.amountOfBarsFlash > 0)
        {
            powerLightActive = !powerLightActive;
            WorldLighting.thisWorldLighting.powerLight.enabled = !WorldLighting.thisWorldLighting.powerLight.enabled;
        }
        if (Time.timeScale == 1 && Input.GetButtonDown("Zoom"))
        {
            isZoomed = !isZoomed;
        }
        if (Time.timeScale == 1 && Input.GetButtonDown("Reload"))
        {
            if (GameController.thisGameController.resourceCount > 0)
            {

                EnhancedFirstPersonController.thisFPC.playerAnim.SetBool("Reload", true);
                reloading = true;
            }

        }

    }
}
