﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player_BulletObject : MonoBehaviour {
    public bool isDead = false;
    IEnumerator PlayParticle(Collision col)//Play blood splatter partilce effect on hit enemy
    {
        Debug.Log("Playing Particle");
        col.gameObject.GetComponent<AI_System>().particleOnHit.GetComponent<ParticleSystem>().Play();
        col.gameObject.GetComponent<AI_System>()._emmisive = col.gameObject.GetComponent<AI_System>().particleOnHit.GetComponent<ParticleSystem>().emission;
        col.gameObject.GetComponent<AI_System>()._emmisive.enabled = true;
        yield return new WaitForSeconds(0.15f);
        col.gameObject.GetComponent<AI_System>().particleOnHit.GetComponent<ParticleSystem>().Stop();
        col.gameObject.GetComponent<AI_System>()._emmisive.enabled = false;
        Destroy(this.gameObject);
        ScoringSystem.thisScoringSystem.currentScore += WaveController.thisWaveController.enemyhitScore;
        col.gameObject.GetComponent<AI_System>().currentHP -= Player_Bullet.thisPlayerBullet.bulletDamage;       
            if (col.gameObject.GetComponent<AI_System>().currentHP == 0)
            {
                col.gameObject.GetComponent<AI_System>().GetComponent<Animator>().SetBool("Death", true);
                col.gameObject.GetComponent<AI_System>().GetComponent<NavMeshAgent>().Stop();
                isDead = true;            
                Destroy(col.gameObject, 2.0f);
                WaveController.thisWaveController.hitSuccess++;
                WaveController.thisWaveController.enemyCount--;
                WaveController.thisWaveController.enemyKilled++;
                ScoringSystem.thisScoringSystem.currentScore += WaveController.thisWaveController.enemyDeathScore;
            }
        }


    void OnCollisionEnter(Collision col)//Bullets collision handler
    {
        if (col.gameObject.tag == "enemy" && col.gameObject != null)
        {
            if (isDead == false)
            {
                StartCoroutine(PlayParticle(col));//Play partilce
            }

        }
        if (col.gameObject.tag == "bullet")
        {
            Destroy(this.gameObject);//If two bulets collide delete bullet
        }
        if(col.gameObject.tag == "Player")//So it doesnt spawn within player
        {
         //make sure we dont spawn inside the player..   
        }
        else
        {
            Rigidbody thisrb;
            thisrb = GetComponent<Rigidbody>();
            thisrb.constraints = RigidbodyConstraints.FreezeAll;
            //Freeze the bullet at location
        }
        Destroy(this.gameObject,2.0f);
    }
}
