﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class EnemyLocator : MonoBehaviour {
    //View Radius of the Player
    public float viewRadius;
    [Range(0,360)]
    //View angle of the Player
    public float viewAngle;
    //Target to find
    public LayerMask targetMask;
    //Obstacles to avoid
    public LayerMask obstacleMask;
    [HideInInspector]
    //Targets visibile
    public List<Transform> visibleTargets = new List<Transform>();

    void Start()
    {
        StartCoroutine("FindTargetsWithDelay",.2f);
    }
    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }
    void FindVisibleTargets()//This will find all visible targets within the defined angle and radius
    {
        visibleTargets.Clear();
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);
        RaycastHit hit;
        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward,dirToTarget) < viewAngle / 2)
            {
                float dstToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position,dirToTarget, out hit, dstToTarget,obstacleMask))
                {
                    visibleTargets.Add(target);
                    targetsInViewRadius[i].GetComponent<AI_System>().isDetected = true;
                    targetsInViewRadius[i].GetComponent<AI_System>().pointToInstantiatePosition = target;
                    //Enemy has been seen and spawn locator
                }

            }
            else if (targetsInViewRadius[i].GetComponent<AI_System>().isDetected == true)
            {
                targetsInViewRadius[i].GetComponent<AI_System>().isDetected = false;
                targetsInViewRadius[i].GetComponent<AI_System>().wasDetected = true;
                //Enemy is seen and was detected

            }

        }
    }
    public Vector3 DirFromAngle(float angleDeg,bool angleIsGlobal)//Get the Direction from the angle
    {
        if (!angleIsGlobal)
        {
            angleDeg += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleDeg * Mathf.Deg2Rad), 0, Mathf.Cos(angleDeg * Mathf.Deg2Rad));
    }
}
