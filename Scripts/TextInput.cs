﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class TextInput : MonoBehaviour {

    // static referecne to this script
    public static TextInput thisTextInput;


    [Header("Input GameObject Settings")]
    //Input field for username
	public InputField inputField;
    //Tect to display name in
    public Text nameDisplay;
    //Button to confir,
	public Button confirmButton;
    [HideInInspector]
    //Players name
    public string playerName;
    //private SQL SQLDatabase;
    //Used when player name is confirmed
    bool startwaiting = false;
    void Awake()
    {
        thisTextInput = this;
    }
	void Start () {
        //IF SQL DATABASE IS USED UNCOMMENT THIS
        //SQLDatabase = GetComponent<SQL>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Return))//When player presses enter for username
        {
            UpdateName();
        }
            if (startwaiting)
        {
            if(Input.GetKeyDown(KeyCode.Space))//When player presses space to go into game
            {
                GameController.thisGameController.introTexts[1].gameObject.SetActive(false);
                Time.timeScale = 1;
                WaveController wave = GetComponent<WaveController>();
                wave.StartSpawn();
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                EnhancedFirstPersonController.thisFPC.enabled = true;
                Player_Bullet.thisPlayerBullet.enabled = true;
                startwaiting = false;
            }
        }
	}
	public void UpdateName()
	{
		/*SQLDatabase.userName*/playerName = inputField.text;
		nameDisplay.text = "Player Name: " + /*SQLDatabase.userName*/playerName;
		if (Regex.Matches(/*SQLDatabase.userName*/playerName, @"[a-zA-Z]").Count >= 1 || Regex.Matches(playerName,@"[0-9]").Count >= 1)//If the user name is valid
		{
			inputField.enabled = false;
			confirmButton.enabled = false;
			confirmButton.gameObject.SetActive(false);
			inputField.gameObject.SetActive(false);
            startwaiting = true;
            GameController.thisGameController.introTexts[0].gameObject.SetActive(false);
            GameController.thisGameController.introTexts[1].gameObject.SetActive(true);

        }
    }
}
