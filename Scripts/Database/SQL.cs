﻿using UnityEngine;
using System.Collections;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System;
using System.Reflection;
[assembly: AssemblyVersionAttribute("4.3.2.1")]
public class SQL : MonoBehaviour
{
    //IF AN SQL DATABASE IS AVAILABLE PUSH WAVE STATS AND PLAYER STATS TO IT 
    string connectionString = "Server=tcp:resistancegamestudios.database.windows.net,1433;Database=TeamGameData;User ID=kyle.norton;Password=Password1234;";
    private WaveController waveObject;
    private Player_Bullet bulletObject;
	public string userName;
    // Use this for initialization
    void Start()
    {
        waveObject = GetComponent<WaveController>();
        bulletObject = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Bullet>();
    }
    public void saveToDB()
    {
        using (SqlConnection dbcon = new SqlConnection(connectionString))
        {

            SqlCommand cmd = new SqlCommand("InsertenemyData", dbcon);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter param = cmd.Parameters.Add("@PlayerId", SqlDbType.NVarChar, 25);
            param.Value = userName;
            param = cmd.Parameters.Add("@Enemy_Killed", SqlDbType.Int);
            param.Value = waveObject.enemyKilled;
            param = cmd.Parameters.Add("@bulletsFired", SqlDbType.Int);
            param.Value =  bulletObject.bulletCount;
            param = cmd.Parameters.Add("@bulletsHit", SqlDbType.Int);
            param.Value = waveObject.hitSuccess;
			param = cmd.Parameters.Add("@PlayerHitSuccess", SqlDbType.Float);
            if (bulletObject.bulletCount <= 0 || waveObject.hitSuccess == 0){
                param.Value = 0;
            }
            else{
                param.Value = bulletObject.bulletCount / waveObject.hitSuccess;
            }
            dbcon.Open();
            cmd.ExecuteNonQuery();
        }

    }
    void OnApplicationQuit()//On quit of game upload stats to SQL DATABASE
    {
        saveToDB();
    }
}
