﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]


public class AudioHandler : MonoBehaviour {
    //static referecne to this scrpit
    static public AudioHandler thisAudioHandler;
    //To reference the audio system array
    private int introSystem = 0, buttonSystem = 1, weaponSystem = 2,attackPlayer =3,isRunning =4;
    //Audio clips to player
    public new AudioSource[] audio;
    // Use this for initialization
    void Awake()
    {
        thisAudioHandler = this;
    }
    void Start () {
        audio[introSystem] = GetComponent<AudioSource>();
        PlayIntroClip();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public void PlayIntroClip()//Play intro Music
    {
        audio[introSystem].Play();
        audio[introSystem].loop = true;
    }
    public void PlayButtonClip()//Play Button Click Sound
    {
        audio[buttonSystem].Play();
    }
    public void PlayWeaponClip()//Play bullet sound
    { 
        audio[weaponSystem].Play();
    }
    public void PlayAttackPlayer()//Play Attack sound
    {
        audio[attackPlayer].Play();
    }
    public void PlayRunning()//Play running clip
    {
        audio[isRunning].Play();
        audio[isRunning].loop = true;
    }
    
}
