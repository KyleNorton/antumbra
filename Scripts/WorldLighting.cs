﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class WorldLighting : MonoBehaviour {
    //static reference to this script
    public static WorldLighting thisWorldLighting;
    [Header("Light Settings")]
    //Flashlight light
    public Light powerLight;
    //Directional Light Light
    public Light directLight;
    //Wether to dim the lights
    public bool dimLights = false;
    //Wether to undim the lights
    public bool unDimLights = false;
    [Header("Skybox Settings")]
    //Dark skybox
    public Material darkSkybox;
    //Light skybox
    public Material lightSkybox;
    void Awake()
    {
        thisWorldLighting = this;
    }
    void Start () {

    }

    // Update is called once per frame
    void Update()
    {
        if (dimLights)//Dim the lights to 0
        {
            directLight.intensity -= Time.deltaTime;
            if (directLight.intensity <= 0.0f)
            {
                dimLights = false;
            }
        }
        if (unDimLights)//Undim lights back to 1
        {
            directLight.intensity += Time.deltaTime;
            if (directLight.intensity >= 1.0f)
            {
                unDimLights = false;
            }
        }
    }
    public void ActivateLights(bool isOn)//Activate worldlights
    {
        if (isOn)
        {
            unDimLights = true;
            RenderSettings.skybox = lightSkybox;
            RenderSettings.defaultReflectionMode = UnityEngine.Rendering.DefaultReflectionMode.Skybox;
        }
        else
        {
            dimLights = true;
            RenderSettings.skybox = darkSkybox;
            RenderSettings.defaultReflectionMode = UnityEngine.Rendering.DefaultReflectionMode.Custom;
        }
    }
}
