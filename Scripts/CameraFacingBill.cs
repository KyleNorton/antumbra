﻿using UnityEngine;
using System.Collections;

public class CameraFacingBill : MonoBehaviour {
    //Main player object
    GameObject player;
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }
    void Update()
    {
        //Flicker object around rotationg the player for use on enemy locator ping
        transform.Rotate(new Vector3(0, 0,transform.rotation.z - player.transform.rotation.y - 180));
    }

}
