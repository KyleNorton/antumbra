﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class AI_System : MonoBehaviour
{

    private Transform myTransform;//Transform of this AI
    [HideInInspector]
    public enum State { Patrol, Engage, Attack };//States of this AI
    [Header("AI Agent Data")]
    #if UNITY_EDITOR
    [ShowOnly]
    #endif
    public bool isDetected = false;//If the Player is Detected
    #if UNITY_EDITOR
    [ShowOnly]
    #endif
    public bool wasDetected = false;//If the Player was Detected
    [HideInInspector]
    private NavMeshAgent agent;//This current agent
    public GameObject meshPrefab;//The Mesh of the AI Prefab
    [Header("AI Agent Settings")]
    #if UNITY_EDITOR
    [ShowOnly]
    #endif
    public State currentState;//Curent state of the AI
    [HideInInspector]
    public enum enemyType { grunt, brute, enforcer };//Types of enemys
    [SerializeField]
    public enemyType currentEnemyType;//Current enemy type
    public enemyType thisCurrentEnemyType//Getter and Setter to chang eenemy types on the fly
    {
        get
        {
            return currentEnemyType;
        }
        set
        {
            currentEnemyType = value;
            _health = (float)WaveController.thisWaveController.GetType().GetField(currentEnemyType.ToString() + "_health").GetValue(WaveController.thisWaveController);//Get the current type of ai and change the health depending
            _damage = (float)WaveController.thisWaveController.GetType().GetField(currentEnemyType.ToString() + "_damage").GetValue(WaveController.thisWaveController);//Get the current type of ai and change the damage depending
            this.GetComponent<NavMeshAgent>().speed = (float)WaveController.thisWaveController.GetType().GetField(currentEnemyType.ToString() + "_moveSpeed").GetValue(WaveController.thisWaveController);//Get the current type of ai and change the move speed depending
            currentHP = _health;//change the health
        }
    }
    #if UNITY_EDITOR
    [ShowOnly]
    #endif
    public float _damage;//damage that this AI does
    #if UNITY_EDITOR
    [ShowOnly]
    #endif
    public float _health;//health this AI has
    #if UNITY_EDITOR
    [ShowOnly]
    #endif
    public float currentHP = 3;//Current AI health
    public float distanceToAttack = 7.50f;
    public float distance;//Distance between player and this AI
    public GameObject pointToInstantiate;//Object which is beung Instantiated
    [HideInInspector]
    public Transform pointToInstantiatePosition;//Where to Instantiate the AI
    public GameObject particleOnHit;//particle to play on hit
    public ParticleSystem.EmissionModule _emmisive;//emmuisive part of the particle
    private GameObject clonePoint;//Cloned AI
    private bool lastPath;//return to last path
    bool dealDamage;//deal damage
    bool engaged = false;//is engaged in player
    GameObject player;//gameobject ref to the player

    // Use this for initialization
    void Awake()
    {
        myTransform = transform;//Setting the transform

    }
    void OnValidate()//FOR USE IN EDITOR---Used to detect changes within the inspector to change enemy type
    {
        if (WaveController.thisWaveController != null)
        {
            thisCurrentEnemyType = currentEnemyType;
        }
    }
    IEnumerator PlayBlood()//Play blood  effect within game and display you have been hit
    {
        GameController.thisGameController.bloodSplatter.GetComponent<UnityEngine.UI.Image>().CrossFadeAlpha(1, 0, false);
        GameController.thisGameController.bloodSplatter.SetActive(true);
        GameController.thisGameController.bloodSplatter.GetComponent<UnityEngine.UI.Image>().CrossFadeAlpha(0, 1.5f,false);
        yield return new WaitForSeconds(1.5f);
        GameController.thisGameController.bloodSplatter.SetActive(false);
    }
    void Start()
    {
       distance = 15f;
       currentState = State.Engage;
       agent = GetComponent<NavMeshAgent>();
       player = GameObject.FindGameObjectWithTag("Player");
        GetComponent<Animator>().SetBool("Walk", true);
        agent.autoBraking = false;
       NextState();

    }
    IEnumerator DealDamage()//What happens when an AI deals damage to the player
    {
        yield return new WaitForSeconds(0.75f);
        GetComponent<AudioSource>().Stop();
        AudioHandler.thisAudioHandler.PlayAttackPlayer();
        EnhancedFirstPersonController.thisFPC.playerAnim.SetBool("Damage", true);
        StartCoroutine(PlayBlood());
        yield return new WaitForSeconds(0.5f);
        EnhancedFirstPersonController.thisFPC.playerAnim.SetBool("Damage", false);
        Health.thisHealth.UpdateHealth(_damage);
        yield return new WaitForSeconds(1.0f);      
        dealDamage = false;
    }
	void FixedUpdate()
	{
        distance = Vector3.Distance(player.transform.position, myTransform.position);
        if (wasDetected)//If enemy was detected
        {
            if (clonePoint != null)
            {
                Destroy(clonePoint);
            }
            clonePoint = Instantiate(pointToInstantiate,new Vector3(pointToInstantiatePosition.position.x,pointToInstantiatePosition.position.y + 3f,pointToInstantiatePosition.position.z), Quaternion.Euler(90,0,-180)) as GameObject;
            Destroy(clonePoint, 5.0f);
            wasDetected = false;
            isDetected = false;
        }
    }
	void onEngage()//Enemy Engage
	{
        if (currentHP > 0)
        {
            if (distance > distanceToAttack)//If enemy is outside attacking range
            {
                if (lastPath)//after attack return to last path
                {
                    GetComponent<AudioSource>().Play();
                    engaged = false;
                    CancelInvoke();
                    agent.Resume();
                    GetComponent<Animator>().SetBool("Attack", false);
                    GetComponent<Animator>().SetBool("Walk", true);
                    lastPath = false;
                }
                agent.SetDestination(player.transform.position);
            }
            else//If enemy is within attacking range
            {
                GetComponent<Animator>().SetBool("Walk", false);
                GetComponent<Animator>().SetBool("Attack", true);
                if (!engaged)//return to last poath if not engaging
                {
                    lastPath = true;
                    agent.Stop();
                    engaged = false;
                }
                if (!dealDamage)//If not dealing damage
                {
                    dealDamage = true;
                    if (Health.thisHealth.playerHealth > 0)
                    {
                        StartCoroutine(DealDamage());
                    }
                }
            }
        }    
      
    }
	IEnumerator EngageState()//Set state to Engage
	{
		Debug.Log("Enemy "  + " is - Engaging...");
		while (currentState == State.Engage)
		{
			onEngage();
			yield return 0;
		}
		Debug.Log("Enemy "  + " is - Exiting Engage..");
		NextState();
	}
	void NextState()//Goes to next state
	{
		string methodName = currentState.ToString() + "State";
		System.Reflection.MethodInfo info =
		GetType().GetMethod(methodName, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
		StartCoroutine((IEnumerator)info.Invoke(this, null));
	}
}
