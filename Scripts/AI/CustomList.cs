﻿//Script name : CustomList.cs
using UnityEngine;
using System;
using System.Collections.Generic; // Import the System.Collections.Generic class to give us access to List<>

public class CustomList : MonoBehaviour
{
    //This script is a Custom List editor to allow access to the wave system within the game and setup each wave manually if wanted
    public static CustomList thisCustomList;
    [Serializable]
        public class EnemySettings
        {
            public float startWait;//wait between start of the wave
            public  List<AI_System.enemyType> enemyTypeAndSize;//The type of enemy this allow dtermine the size of the wave
        }
    public EnemySettings[] EnemySettingsArray;//For creation of multiple waves
    void Awake()
    {
        thisCustomList = this;
    }
}

