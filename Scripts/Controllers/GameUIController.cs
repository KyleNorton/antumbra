﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameUIController : MonoBehaviour {
    //static reference to this script
    public static GameUIController thisGameUIController;
    //Energy bar Image objects
    public Image[] EnergyBarObjects;
    //Flashlight bar Image objects
    public Image[] FlashLightBarObjects;
    //Health bar Image objects
    public Image[] healthBarObjects;
    void Awake()
    {
        thisGameUIController = this;
    }
    public void UpdateEnergyBars()//Set Energy bars to full
    {
        foreach (var item in EnergyBarObjects)
        {
            item.fillAmount = 1;
            item.gameObject.SetActive(true);
        }
    }
    public void UpdateFlashLightBars()//Set Flashlight bars to full
    {
        foreach (var item in FlashLightBarObjects)
        {
            item.fillAmount = 1;
            item.gameObject.SetActive(true);
        }
        Energy.thisEnergy.amountOfBarsFlash = 9;
    }

}
