﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;

public class WaveController : MonoBehaviour
{
    //static reference to this script
    public static WaveController thisWaveController;   
    [Header("Enemy Settings")]
    //Enemy object we spawn
    public GameObject prefab;
    //Where to spawn the enemys
    public Transform[] spawnValues;
    //Score given on death of enemy
    public float enemyDeathScore = 50f;
    //Score given on hit of enemy
    public float enemyhitScore = 15f;
    //---Enemy Types Health,Damage & Move Speed ---  
    [Header("Grunt Stats")]
    public float grunt_health;
    public float grunt_damage;
    public float grunt_moveSpeed;
    [Header("Brute Stats")]
    public float brute_health;
    public float brute_damage;
    public float brute_moveSpeed;
    public Material brute_Material;
    [Header("Enforcer Stats")]
    public float enforcer_health;
    public float enforcer_damage;
    public float enforcer_moveSpeed;
    public Material enforcer_Material;
    //----------------------------------------------
    [Header("Wave Settings")]
    //Current Wave
	public int waveCount = 1;
    //Spawning WaveS?
	private bool spawning = true;
    //Wave Incoming Text on/off
    private bool updateWaveText = false;
    #if UNITY_EDITOR
    [ShowOnly]
    #endif
    //enemy count
    public int enemyCount;
    #if UNITY_EDITOR
    [ShowOnly]
    #endif
    //enemys killed
    public int enemyKilled;
    #if UNITY_EDITOR
    [ShowOnly]
    #endif
    //Total amount of hits
    public int hitSuccess;
    //Width on the x-axis between eah=ch enemy spawned
    public float spawnXwidth = 5.0f;
    [Header("Waves Setup")]
    public int amountofwaves = 50;
    [Header("Wave Game Object Settings")]
    //Wave Incomning Text
    public Text Wave;
    //Count of Wave Text
    public Text countWave;
    [Header("Resource Settings")]
    [HideInInspector]
    //Spawned resources
    public int spawnedResources;
    [HideInInspector]
    //Iterate over spawning
    public int spawnPointIterator = 0;
    //Amount of resources to spawn
    public int resourcesToSpawn = 8;
    //time to wait(used for text)
    private float timeToWait;

    //enemys this wave
    private int spawnedthiswave;
    void Awake()
    {
        thisWaveController = this;
    }
	//Update is called once per frame
	void Update()
	{
		CheckSpawn();//Check if there are still enemys
        if(updateWaveText)//If new wave is ready to come
        {
            timeToWait -= Time.deltaTime;
            Wave.text = "Wave " + waveCount + " Incoming In - " + ((int)timeToWait);
            if (timeToWait <= 0.0f)
                updateWaveText = false;
        }
    }
	void CheckSpawn()//Check if there are enemys spawned
	{
		if (spawning == false)
		{
			if (enemyCount <= 0)//Goto next wave
			{
                enemyCount = 0;
                spawnedthiswave = 0;
                waveCount++;
				spawning = true;
				StartCoroutine(SpawnWaves());//Spawn waves			
			}
		}

	}
    public void StartSpawn()//Start spawning wave
    {
        StartCoroutine(SpawnWaves());
    }
	int spawningWave()//Spawn resources and enemys
	{
        SaveLoad saver = GameObject.FindGameObjectWithTag("GameController").GetComponent<SaveLoad>();
        saver.Save();
        #region SpawnResources
        while (spawnedResources < resourcesToSpawn)//Spawn resources until max is reached
        {
            Resource_Management.thisResource_Management.SpawnResources();
        }
        #endregion
        #region SpawnEnemy
        if (spawnValues.Length > 0)//if there are spawners
        {
            GameObject clone = Instantiate(prefab, spawnValues[UnityEngine.Random.Range(0, spawnValues.Length)].localPosition,Quaternion.identity) as GameObject;
            clone.GetComponent<AI_System>().thisCurrentEnemyType = CustomList.thisCustomList.EnemySettingsArray[waveCount - 1].enemyTypeAndSize[spawnedthiswave];
            if(clone.GetComponent<AI_System>().thisCurrentEnemyType == AI_System.enemyType.brute)//if enemy is brute change mats and size
            {
                clone.GetComponent<AI_System>().meshPrefab.GetComponent<Renderer>().material = brute_Material;
                clone.transform.localScale = new Vector3(1.25f, 1.25f, 1.25f);
            }
            if (clone.GetComponent<AI_System>().thisCurrentEnemyType == AI_System.enemyType.enforcer)//if enemy is enforcer change mats and size
            {
                clone.GetComponent<AI_System>().meshPrefab.GetComponent<Renderer>().material = enforcer_Material;
                clone.transform.localScale = new Vector3(1.8f, 1.8f, 1.8f);
            }
            enemyCount++;
            spawnedthiswave++;
            //Save Progress at the beggining of every wave
        }
        #endregion
        return 0;
	}

	IEnumerator SpawnWaves()//Setup each round and then call for a spawn of wave
	{
        if (spawnedResources > 0)//delete resources before each wave
        {
            foreach (var item in Resource_Management.thisResource_Management.spawnedObjects)
            {
                Destroy(item);
            }
            spawnedResources = 0;
        }
        GameController.thisGameController.flashlightBar.GetComponentInChildren<Text>().text = "FlashLight";
        GameUIController.thisGameUIController.UpdateFlashLightBars();//Reload flashlight
        timeToWait = CustomList.thisCustomList.EnemySettingsArray[waveCount - 1].startWait;
        updateWaveText = true;
        Debug.Log("Waiting for seconds...");//--START--Wait then spawn wave
        WorldLighting.thisWorldLighting.ActivateLights(true);
        yield return new WaitForSeconds(CustomList.thisCustomList.EnemySettingsArray[waveCount - 1].startWait);
        WorldLighting.thisWorldLighting.ActivateLights(false);
        Debug.Log("Stopped waiting");//--END
        Wave.text = null;
        while (spawning)//While there are still enemies to spawn
	    {
		    for (int i = 0; i < CustomList.thisCustomList.EnemySettingsArray[waveCount - 1].enemyTypeAndSize.Count; i++)
			{
				if (spawnedthiswave >= CustomList.thisCustomList.EnemySettingsArray[waveCount-1].enemyTypeAndSize.Count)
				{
					spawning = false;
                       break;
				}
				spawningWave();
                yield return new WaitForSeconds(0.5f);
            }
		}
	}

}
