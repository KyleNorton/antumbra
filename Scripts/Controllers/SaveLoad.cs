﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class SaveLoad : MonoBehaviour {

    public void Save()//Save Game
    {
        Debug.Log("Saving Game");
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fStream = File.Create(Application.persistentDataPath + "/savefile.dat");
        SaveManager saver = new SaveManager();
        saver.waveAt = WaveController.thisWaveController.waveCount;
        saver.playerName = TextInput.thisTextInput.nameDisplay.text;
        foreach (var item in GameUIController.thisGameUIController.EnergyBarObjects)
        {
            saver.energyPlayer.Add(item.fillAmount);
        }//saving  energy
        foreach (var item in GameUIController.thisGameUIController.healthBarObjects)
        {
            saver.healthPlayer.Add(item.fillAmount);
        }//saving health
        foreach (var item in GameUIController.thisGameUIController.FlashLightBarObjects)
        {
            saver.playerPower.Add(item.fillAmount);
        }//saving player power
        saver.currentPlayerScore = ScoringSystem.thisScoringSystem.currentScore;
        //Rest of save info
        bf.Serialize(fStream, saver);
        fStream.Close();
    }
    public void Load()//Load Game
    {
        if (File.Exists(Application.persistentDataPath + "/savefile.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fstream = File.Open(Application.persistentDataPath + "/savefile.dat", FileMode.Open);
            SaveManager saver = (SaveManager)bf.Deserialize(fstream);
            fstream.Close();
            WaveController.thisWaveController.waveCount = saver.waveAt;
            TextInput.thisTextInput.nameDisplay.text = saver.playerName;
            int k = 0;
            foreach (var item in saver.energyPlayer)
            {
                GameUIController.thisGameUIController.EnergyBarObjects[k].fillAmount = saver.energyPlayer[k];
                k++;
            }
            Energy.thisEnergy.amountOfBars = k - 1;
            //loading energy
            int j = 0;
            foreach (var item in saver.energyPlayer)
            {
               GameUIController.thisGameUIController.healthBarObjects[j].fillAmount = saver.healthPlayer[j];
               j++;
            }//Loading health
            Health.thisHealth.amountOfBars = j - 1;
            int l = 0;
            foreach (var item in saver.energyPlayer)
            {
                GameUIController.thisGameUIController.FlashLightBarObjects[l].fillAmount = saver.playerPower[l];
                l++;
            }//Loading flashlight
            Energy.thisEnergy.amountOfBarsFlash = l - 1;
            ScoringSystem.thisScoringSystem.currentScore = saver.currentPlayerScore;
        }
    }
    [Serializable]
    class SaveManager
    {
        public int waveAt;//Wave player was at
        public string playerName;//Name of the player
        public List<float> healthPlayer = new List<float>();//Health of the player
        public List<float> energyPlayer = new List<float>();//Energy for the player
        public List<float> playerPower = new List<float>();//FLashlight amount for the player
        public float currentPlayerScore;//Score for the player
    }

}
