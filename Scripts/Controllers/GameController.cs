﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController thisGameController;
    [Header("Wave Gameobjects")]
    public Text waveIncoming;//Text to display wave is coming
    [Header("Scoring System ")]
    public GameObject scoringBoard;//Game object on which the Score is kept and displayed
    public GameObject finalScoreBoard;//Game object where the final score is put and displayed
    public float scorePerSecond = 150f;//amount to add to score per second
    public float scorePerSecondDelay = 2.0f;//delay between each score addition
    private float timeDelayScore;//Time between each score
    [Header("Player Gameobjects")]
    public GameObject energyBar;//The energy bar
    public GameObject flashlightBar;//The flashlight bar
    public GameObject healthBar;//The health bar
    public GameObject bloodSplatter;//Blood effect on screen when hit
    [Header("Name GameObjects")]
    public InputField inputPlayerName;//Text box where to enter player name
    public Text playerName;//Text to display player name
    public Button confirmButton;//The confirm button
    [Header("Menu GameObjects")]
    public GameObject scoringSystemObject;//Turn on the scoring system within the game
    public Image introImage;//Intro / Splash image on menu screen
    public GameObject settingsMenu;//The settings menu within the game
    public GameObject mainMenu;//Main menu 
    public GameObject pauseMenu;//Pause menu
    public GameObject controlsMenu;//Controls menu
    public GameObject creditsMenu;//Credits menu
    public GameObject deathScreen;//Death menu
    public Button continueButton;//Continue button
    public Slider volumeBar;//Volume bar
    [Tooltip("Slider value should have a min value of 50 & a max value of 500")]
    public Slider mouseSensitivity;//Mouse sensitivity bar
    public Dropdown[] settingsDropdownComponents;//Settings bars
    Resolution[] resolutions;//Resolotuon dropdowns
    [HideInInspector]
    public string[] currentResOption;//current resolution slection
    [HideInInspector]
    public string[] qualityOptions;//current quality selection
    [Header("Resource GameObjects")]
    #if UNITY_EDITOR
    [ShowOnly]
    #endif
    public int resourceCount;//Amount of resources
    public Text resourceText;//Resource text
    public Text resourceLimit;//Max resource text
    //---START---Resource Pickup Circle
    public GameObject LoadingCircle;//Loading circle object
    public Transform LoadingBar;//THe bar around the object
    public Transform TextIndicator;//Indicator of pickup
    public Transform TextLoading;//percentage of pickup
    [SerializeField]
    public float currentAmmount;//current percentage
    [SerializeField]
    public float speed;//speed to pick up
    [HideInInspector]
    public bool resourcePickUp = false;
    [HideInInspector]
    public float pickupTime = 0.0f;//pickup time
    //---END--- Resource Pickup Circle
    private bool inMenu = false;//in menu?   
    public GameObject[] introTexts;//Introduction objects
    void Awake()
    {
        thisGameController = this;
    }

    void Start()
    {
        currentResOption = new string[20];
        qualityOptions = new string[8];
        Time.timeScale = 0;
        QualitySettings.vSyncCount = 0;
        resolutions = Screen.resolutions;
        for (int i = 0; i < resolutions.Length; i++)//Setup res dropdowns
        {
            settingsDropdownComponents[0].options.Add(new Dropdown.OptionData(ResToString(resolutions[i])));
            settingsDropdownComponents[0].options[i].text = ResToString(resolutions[i]);
            settingsDropdownComponents[0].value = i;
            settingsDropdownComponents[0].onValueChanged.AddListener(delegate{Screen.SetResolution(resolutions[settingsDropdownComponents[0].value].width,resolutions[settingsDropdownComponents[0].value].height, true);});//Adds delegate to each res option
        }
        for (int i = 0; i < resolutions.Length; i++)//Setup res drop down text
        {
            currentResOption[i] = settingsDropdownComponents[0].options[i].text;
            if (Screen.currentResolution.width + " x " + Screen.currentResolution.height == currentResOption[i])
            {
                settingsDropdownComponents[0].value = i;
            }
        }
        string[] names = QualitySettings.names;
        for (int i = 0; i < names.Length; i++)//Setup qaulity dropdowns
        {
            settingsDropdownComponents[2].options.Add(new Dropdown.OptionData(names[i]));
            settingsDropdownComponents[2].onValueChanged.AddListener(delegate { QualitySettings.SetQualityLevel(i, true); });
            qualityOptions[i] = settingsDropdownComponents[2].options[i].text;
        }
        settingsDropdownComponents[2].Select();
        settingsDropdownComponents[2].RefreshShownValue();
        settingsDropdownComponents[0].Select();
        settingsDropdownComponents[0].RefreshShownValue();
        //Select menu options  to be the ones that were initally set on startup
        foreach (var item in introTexts)
        {
            item.gameObject.SetActive(false);
        }
        continueButton.gameObject.SetActive(true);
        //Setting up all menu dropdown boxes with default values aswell as buttons
        Player_Bullet.thisPlayerBullet = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Bullet>();
        mouseSensitivity.value = EnhancedFirstPersonController.thisFPC.mouseSensitivity * 10;
        volumeBar.value = 1.0f;

    }


    string ResToString(Resolution res)//Resoultion to string
    {
        return res.width + " x " + res.height;
    }
    void Update()
    {
        if (Time.timeScale == 1 && Input.GetButton("inGameMenu") && inMenu == false)//Open the pause menu
        {
            bloodSplatter.SetActive(false);
            pauseMenu.gameObject.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Time.timeScale = 0;
            inMenu = true;
        }
        if (Time.timeScale == 1 && Input.GetButton("inGameMenu") && inMenu == true)//Close the pause menu
        {
            bloodSplatter.SetActive(true);
            OnResume();
            inMenu = false;
        }
        if (Time.timeScale == 1)
        {
            if (timeDelayScore >= scorePerSecondDelay)//Apply score
            {
                ScoringSystem.thisScoringSystem.currentScore += (int)(scorePerSecond);
                timeDelayScore = 0.0f;
            }
            else
            {
                timeDelayScore += Time.deltaTime;//delay between each score update
            }

        }

    }
    public void OnStart()//On start of game
    {
        StartCoroutine(OnStartCoroutine());
    }
    IEnumerator OnStartCoroutine()//Setup scene with menu
    {
        introTexts[0].SetActive(true);
        mainMenu.gameObject.SetActive(false);
        introImage.gameObject.SetActive(false);
        waveIncoming.gameObject.SetActive(true);
        healthBar.gameObject.SetActive(true);
        flashlightBar.SetActive(true);
        energyBar.SetActive(true);
        energyBar.gameObject.SetActive(true);
        inputPlayerName.gameObject.SetActive(true);
        playerName.gameObject.SetActive(true);
        confirmButton.gameObject.SetActive(true);
        scoringSystemObject.SetActive(true);
        resourceText.gameObject.SetActive(true);
        playerName.gameObject.SetActive(true);
        yield return 0;
    }
    public void OnResume()//On game resume
    {
        Time.timeScale = 1;
        pauseMenu.gameObject.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    public void OnRestart()//On game restart
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
    public void OnOptions()//On options entry
    {
        settingsMenu.gameObject.SetActive(true);
        if (inMenu == false)
            mainMenu.gameObject.SetActive(false);
        else
            pauseMenu.gameObject.SetActive(false);
    }
    public void OnQuit()//On game quit
    {
        Application.Quit();
    }
    public void OnSettingsBack()//On settings return
    {
        if (inMenu == false)
        {
            settingsMenu.gameObject.SetActive(false);
            mainMenu.gameObject.SetActive(true);
        }
        else if (inMenu == true)
        {
            settingsMenu.gameObject.SetActive(false);
            pauseMenu.gameObject.SetActive(true);
        }
    }
    public void OnControlsBack()//On controls list return
    {
        if (inMenu == false)
        {
            controlsMenu.gameObject.SetActive(false);
            mainMenu.gameObject.SetActive(true);
        }
        else if (inMenu == true)
        {
            controlsMenu.gameObject.SetActive(false);
            pauseMenu.gameObject.SetActive(true);
        }
    }
    public void OnCreditsBack()//On Credits return
    {
        if (inMenu == false)
        {
            creditsMenu.gameObject.SetActive(false);
            mainMenu.gameObject.SetActive(true);
        }
        else if (inMenu == true)
        {
            creditsMenu.gameObject.SetActive(false);
            pauseMenu.gameObject.SetActive(true);
        }
    }
    public void Vsync()//Vysnc Entry
    {
        if (settingsDropdownComponents[1].value == 0)
        {
            QualitySettings.vSyncCount = 1;
            Debug.Log("Vsync On");
        }
        if (settingsDropdownComponents[1].value == 1)
        {
            QualitySettings.vSyncCount = 0;
            Debug.Log("Vsync Off");
        }
    }
    public void onControls()//On Controls Entry
    {
        settingsMenu.gameObject.SetActive(false);
        controlsMenu.gameObject.SetActive(true);
    }
    public void onCredits()//On Credits Entry
    {
        settingsMenu.gameObject.SetActive(false);
        creditsMenu.gameObject.SetActive(true);
    }
    public void OnContinue()//On Continue Entry
    {
        SaveLoad saver = GetComponent<SaveLoad>();
        saver.Load();
        Time.timeScale = 1;
        WaveController wave = GetComponent<WaveController>();
        wave.StartSpawn();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        mainMenu.gameObject.SetActive(false);
        waveIncoming.gameObject.SetActive(true);
        healthBar.gameObject.SetActive(true);
        energyBar.SetActive(true);
        playerName.gameObject.SetActive(true);
        EnhancedFirstPersonController.thisFPC.enabled = true;
        Player_Bullet.thisPlayerBullet.enabled = true;
        scoringSystemObject.SetActive(true);
        introImage.gameObject.SetActive(false);
        resourceText.gameObject.SetActive(true);
        playerName.gameObject.SetActive(true);
    }

    public void OnVolume()//On Volume change
    {
        foreach (var item in AudioHandler.thisAudioHandler.audio)
        {
            item.volume = volumeBar.value;
        }
        AudioListener.volume = volumeBar.value;
    }
    public void OnMouseSensitivity()//On Mouse sens change
    {
        EnhancedFirstPersonController.thisFPC.mouseSensitivity = mouseSensitivity.value / 10;
    }
}
